

import java.util.Scanner;

//this program demonstrates working of simple calculator. Which has the following methods
//1) Add
//2)Subtract
//3)Divide
//4)Multiply
public class SimpleCalculator {
	static int res = 0;

	public static void add(int a, int b) {// Suriya

		res = a + b;
		System.out.println("Addition of two numbers is:" + res);
	}

	public static void multiply(int a, int b) {
		res = a * b;
		System.out.println("Product of two number is:" + res);
	}

	// Hunter
	public static void divide(int a, int b) {
		if (b == 0) {
			System.err.println("The second number can not be 0!");
			return;
		}
		res = a / b;
		System.out.println("Division of two number is:" + res);
	}
	
	// Abhik
		public static void sub(int a, int b) {
			res = a - b;
			System.out.println("The Subtraction of two numbers is:" + res);
		}

	public static void main(String[] args) {
		int input = 0;
		int num1 = 0;
		int num2 = 0;
		System.out.println("Simple Calculator");
		System.out.println("1.Add");
		System.out.println("2.Subtract");
		System.out.println("3.Divide");
		System.out.println("4.Multiply");
		Scanner sc = new Scanner(System.in);
		input = sc.nextInt();
		System.out.println("Enter the first number");
		num1 = sc.nextInt();
		System.out.println("Enter the second number");
		num2 = sc.nextInt();

		switch (input) {
		case 1:
			add(num1, num2);
			break;
		case 2:
			sub(num1, num2);
			break;
		case 3:
			divide(num1, num2);
			break;
		case 4:
			multiply(num1, num2);
			break;

		}
	}
}
